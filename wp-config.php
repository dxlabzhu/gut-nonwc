<?php
/**
 * A WordPress fő konfigurációs állománya
 *
 * Ebben a fájlban a következő beállításokat lehet megtenni: MySQL beállítások
 * tábla előtagok, titkos kulcsok, a WordPress nyelve, és ABSPATH.
 * További információ a fájl lehetséges opcióiról angolul itt található:
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 *  A MySQL beállításokat a szolgáltatónktól kell kérni.
 *
 * Ebből a fájlból készül el a telepítési folyamat közben a wp-config.php
 * állomány. Nem kötelező a webes telepítés használata, elegendő átnevezni
 * "wp-config.php" névre, és kitölteni az értékeket.
 *
 * @package WordPress
 */

// ** MySQL beállítások - Ezeket a szolgálatótól lehet beszerezni ** //
/** Adatbázis neve */
define( 'DB_NAME', 'gut-nonwc' );

/** MySQL felhasználónév */
define( 'DB_USER', 'root' );

/** MySQL jelszó. */
define( 'DB_PASSWORD', '' );

/** MySQL  kiszolgáló neve */
define( 'DB_HOST', 'localhost' );

/** Az adatbázis karakter kódolása */
define( 'DB_CHARSET', 'utf8mb4' );

/** Az adatbázis egybevetése */
define('DB_COLLATE', '');

/**#@+
 * Bejelentkezést tikosító kulcsok
 *
 * Változtassuk meg a lenti konstansok értékét egy-egy tetszóleges mondatra.
 * Generálhatunk is ilyen kulcsokat a {@link http://api.wordpress.org/secret-key/1.1/ WordPress.org titkos kulcs szolgáltatásával}
 * Ezeknek a kulcsoknak a módosításával bármikor kiléptethető az összes bejelentkezett felhasználó az oldalról.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', '3V=0]r1y7a}17 GaG2yN(Wxc,!8i2z_EaEZ9/ }[>dhSCjrW: V[n5XNK~=)*4+5' );
define( 'SECURE_AUTH_KEY', '&->Z<H)h(4|.4^it! ED /nM>n^>P0JmpTIa)sf:FzI)QiSoUgYHdq*Z`Tq~?0bz' );
define( 'LOGGED_IN_KEY', '3}]qXj/A&z(@9oLJ> pA-)/!-83C~95{<8r<-)@RUGtVA%;{VaeFP>qn*8$da^R3' );
define( 'NONCE_KEY', '14S+y!w+X?18V,=%P=&Rkh]V->k2+F3>$Q.Afe.6_DOtl{ojX(`pmqsSXURoY[#E' );
define( 'AUTH_SALT',        '55+-7~,4kbA*2lW)fmp@7npNk7gdRnit<ql^{vE*b[sBn}6{zh7_yN2cKlu:VlW:' );
define( 'SECURE_AUTH_SALT', '7{[[]-)bi_0h|6Ji~YMaFQ@Dl7@Q0&!H~X4U&i-,<HD%gi@)W(E)+,~Ya.wf~H/B' );
define( 'LOGGED_IN_SALT',   '/9qStY~FuuQ+}8=VyA@RV0 <29>8p=LSLUM Lj3gTV-/n$w-CPT_3ZJ+`pkx3uTA' );
define( 'NONCE_SALT',       'R6bjpSnPW%@2u)CMR8ID=#qc8suA07}GRq{_ lz8|;d3U1sP/j[@Ln|NxH*q]%K:' );

/**#@-*/

/**
 * WordPress-adatbázis tábla előtag.
 *
 * Több blogot is telepíthetünk egy adatbázisba, ha valamennyinek egyedi
 * előtagot adunk. Csak számokat, betűket és alulvonásokat adhatunk meg.
 */
$table_prefix = 'dxgt_';

/**
 * Fejlesztőknek: WordPress hibakereső mód.
 *
 * Engedélyezzük ezt a megjegyzések megjelenítéséhez a fejlesztés során.
 * Erősen ajánlott, hogy a bővítmény- és sablonfejlesztők használják a WP_DEBUG
 * konstansot.
 */
define('WP_DEBUG', false);

/* Ennyi volt, kellemes blogolást! */

/** A WordPress könyvtár abszolút elérési útja. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Betöltjük a WordPress változókat és szükséges fájlokat. */
require_once(ABSPATH . 'wp-settings.php');
