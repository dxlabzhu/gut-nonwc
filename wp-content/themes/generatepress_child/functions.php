<?php
/**
 * GeneratePress child theme functions and definitions.
 *
 * Add your custom PHP in this file.
 * Only edit this file if you have direct access to it on your server (to fix errors if they happen).
 */

 /** LOGIN STYLING **/

 function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/css/style-login.css' );
    //wp_enqueue_script( 'custom-login', get_stylesheet_directory_uri() . 'css/style-login.js' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

/** BREAK POINTS FOR RESPONSIVE BLOCK CONTROL **/

function override_responsive_block_control_breakpoints($break_points) {
     $break_points['base'] = 0;
     $break_points['mobile'] = 768;
     $break_points['tablet'] = 1024;
     $break_points['desktop'] = 1366;
     $break_points['wide'] = 1920;

     return $break_points;
 }

 add_filter('responsive_block_control_breakpoints', 'override_responsive_block_control_breakpoints');


