=== WordPress Slider Plugin - Block Slider ===
Contributors: munirkamal, freemius
Donate link: https://wpblockslider.com/donate/
Tags: wordpress slider, gutenberg, gutenberg slider, slider block, gutenberg block, slider plugin, carousel, slider, image slider, responsive slider, slideshow, fullwidth slider, post slider, horizontal slider, vertical slider, photo slider, video slider, responsive slides
Requires at least: 5.0
Tested up to: 5.8
Stable tag: 1.2.9
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-1.0.6.html

The slider plugin for WordPress Gutenberg editor. Build sliders directly within Gutenberg editor live. Add any WordPress blocks to each slide.


== Description ==

= WordPress Slider Plugin - Block Slider =

Existing slider plugins does not allow you to build your sliders within Gutenberg WordPress Editor. They offer mostly custom made slide builder (or no slide builder) which was a great feature earlier. But with the Gutenberg editor, you should be able to build your slides directly in the Gutenberg editor.

That’s why we built <a href="http://wpblockslider.com/" rel="friend" title="Block Slider">Block Slider</a>.This is the next generation **WordPress slider plugin**. You can easily build your slides by adding any gutenberg WordPress blocks within the Gutenberg Editor. You do not need to leave the editor to build your sliders using some other drag & drop slide builder. Now you can build your slides within Gutenberg editor using Block Slider plugin.

Block Slider adds a Slider Block in Gutenberg and allows makes it possible for you to add any Gutenberg block(s) within the slides, nice and easy. You can add multiple blocks within a slide, and you can customize the slider functionalities and style within the Gutenberg Editor. Block slider.

= Your Feedback Matters =

Please pass your suggestions and feedback to us via the support forum to improve the plugin.

## Template and Pattern Library

Block Slider now includes access to the Extendify template and pattern library that can be accessed by clicking the “Library” button in the menu bar of the editor. We provide a variety of templates and patterns to serve different use cases and are adding more templates every week.

Note: Templates and patterns rely on blocks and styling applied by the Editor Plus plugin. The Editor Plus plugin is required when importing a template or pattern. If you uninstall Editor Plus, the templates and patterns will still display on your site, but the styling may be affected. Additionally, stock images included in the templates are hosted on GutenbergHub.com. We recommend replacing the images or adding them to your media library directly.

## Privacy

Block Slider uses a custom API to fetch templates and patterns from the Extendify template and pattern library. To improve the service and stability we store logs which may contain the following:

* browser type
* referring site
* date and time of request
* category selection or search term
* anonymized IP address

API requests are only made when a user clicks on the Library button.

For more details on our privacy policy: https://wpblockslider.com/privacy-policy/
For more details on our terms of service: https://wpblockslider.com/terms-of-service/

== Installation ==

1. Upload the entire plugin folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
Once Activated, you will find 'Block Slider' block in the Gutenberg editor.
For basic usage, you can also have a look at the plugin website [Block Slider](https://wpblockslider.com/).

== Screenshots ==

1. WordPress Slider Plugin - Block Slider
2. Adding New Slide
3. Add any WordPress blocks to slide.
4. Slider Settings

== Changelog ==

For more information, visit [Block Slider](https://wpblockslider.com).

= 1.2.9 (14 sep 2021) =
* Improved: Templates Library

= 1.2.8 (26 aug 2021) =
* Improved: Templates Library

= 1.2.7 (12 aug 2021) =
* Improved: Library

= 1.2.6 (13 july 2021) =
* Improved: Library
* Fixed: Bugs

= 1.2.5 (8 july 2021) =
* Improved: Library

= 1.2.4 (29 june 2021) =
* New: Toggle to enable/disable Extendify library
* Improved: Updates to the pattern and template library

= 1.2.3 (17 june 2021) =
* New: Improve Template library

= 1.2.2 (24 may 2021) =
* Template library improvement

= 1.2.1 (11 may 2021) =
* Bug Fixes

= 1.2.0 (21 Apr 2021) =
* New: Access to the Extendify template and pattern library

= 1.1.1 (19 Aug 2020) =
* Fixed: Issue with previous version update

= 1.1.1 (18 Aug 2020) =
* Fixed: Add Slide Bug

= 1.1.0 (15 Aug 2020) =

* Add Compatibility with WordPress 5.0.
* Fixed: Bugs

= 1.0.9 (16 Mar 2020) =

* Many features added.

= 1.0.8 (17 Feb 2020) =

* A lot of features added.

= 1.0.0 (18 Nov 1.0.68) =

* Initial release with the basic functionality.
