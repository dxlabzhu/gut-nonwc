<?php

/**
 * Plugin Name: HelloPack
 * Plugin URI: https://hellowp.io
 * Description: Updates for premium addons
 * Version: 1.1.9
 * Tested up to: 5.5
 * Author: HelloWP Ltd
 * Author URI: https://hellowp.io
 * Text Domain: hellopack
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Network: True
 */

defined('WPINC') || die("-1");


function HP_LANG()
{
    load_plugin_textdomain('hellopack', false, basename(dirname(__FILE__)) . '/languages/');
}
add_action('plugins_loaded', 'HP_LANG');

/** Adding Overrides */
include_once dirname(__FILE__) . '/includes/overrides/includes.php';

defined('HP_UPDATER_VERSION') || define('HP_UPDATER_VERSION', '1.1.9');

defined('HP_UPDATER_NAME') || define('HP_UPDATER_NAME', 'hellopack-updater');

if (!defined('HP_UPDATER_PATH')) {
    define('HP_UPDATER_PATH', plugin_dir_path(__FILE__));
}

if (!defined('HP_UPDATER_URL')) {
    define('HP_UPDATER_URL', plugin_dir_url(__FILE__));
}

if (!defined('HP_UPDATER_INC')) {
    define('HP_UPDATER_INC', plugin_dir_path(__FILE__) . 'includes/');
}

if (!defined('HP_UPDATER_BASENAME')) {
    define('HP_UPDATER_BASENAME', plugin_basename(__FILE__));
}

if (!defined('HP_UPDATER_STATIC_PATH')) {
    define('HP_UPDATER_STATIC_PATH', plugin_dir_path(__FILE__) . 'static/');
}

if (!defined('HP_UPDATER_STATIC_URL')) {
    define('HP_UPDATER_STATIC_URL', plugin_dir_url(__FILE__) . 'static/');
}

defined('HP_UPDATER_API_URL') || define('HP_UPDATER_API_URL', 'https://hellopack.wp-json.app/');

define('HP_UPDATER_WP_JSON_URL', 'https://api.wp-json.app/');

defined('HPACKS_RENAME_PLUGINS') || define('HPACKS_RENAME_PLUGINS', true);

if (!defined('HP_UPDATER_LOG_DIR')) {
    $up_dir = wp_upload_dir();

    define('HP_UPDATER_LOG_DIR', $up_dir['basedir'] . '/hellopack-logs/');
}

register_activation_hook(__FILE__, 'hellopack_updater_activate');

register_deactivation_hook(__FILE__, 'hellopack_updater_deactivate');


function hellopack_updater_activate()
{
    require_once HP_UPDATER_INC . 'class-hellopack-activation.php';
    HPack_Updater_Activator::activate();
}

function hellopack_updater_deactivate()
{
    require HP_UPDATER_INC . 'deactivate.php';
    require_once HP_UPDATER_INC . 'class-hellopack-deactivation.php';
    HPack_Updater_Deactivator::deactivate();
}

/**
 * Starts the excution of the plugin here
 */
require HP_UPDATER_INC . 'class-hellopack-updater.php';
require HP_UPDATER_INC . 'serials.php';

// Include modules

require HP_UPDATER_INC . 'modules.php';

/**
 * @return HPack_Updater
 */
function HPMain()
{
    return HPack_Updater::instance();
}

function starts_hellopack()
{
    HPMain()->run();
}
starts_hellopack();

$GLOBALS['hellopack'] = HPMain();
