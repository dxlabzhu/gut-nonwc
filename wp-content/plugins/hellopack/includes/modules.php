<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if (get_option('hellopack_updater_activated') == 'Activated') {
    foreach (glob(HP_UPDATER_INC . '/modules/*.php') as $file) {
        require $file;
    }
}
