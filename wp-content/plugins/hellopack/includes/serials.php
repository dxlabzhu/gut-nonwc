<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

include_once ABSPATH . 'wp-admin/includes/plugin.php';

if (is_plugin_active('userpro/index.php')) {
    function userpro_set_options($option, $newvalue)
    {
        $settings = get_option('userpro');
        $settings[$option] = $newvalue;
        update_option('userpro', $settings);
    }

    update_option('userpro_trial', 0);
    update_option('userpro_activated', 1);
    userpro_set_options('userpro_code', 1);
    userpro_set_options('envato_token', 1);
}

if (is_plugin_active('woocommerce-appointments/woocommerce-appointments.php')) {
    update_option('bizz_woocommerce_appointments_license_active', 'valid');
}

if (is_plugin_active('product-extras-for-woocommerce/product-extras-for-woocommerce.php')) {
    update_option('pewc_license_status', 'valid');
    update_option('pewc_payment_id', '82200');
    update_option('pewc_license_id', '1100');
    update_option('pewc_license_level', '0');
    update_option('pewc_license_key', '919378767451da');
    delete_option('pewc_license_status_message');

    if (get_option('pewc_license_level') == 1) {
        update_option('pewc_license_level', '0');
    }
}

if (is_plugin_active('sfwd-lms/sfwd_lms.php')) {
    define('LEARNDASH_UPDATES_ENABLED', 'enabled');
}

if (is_plugin_active('product-extras-for-woocommerce/product-extras-for-woocommerce.php')) {
    update_option('pewc_license_status', 'valid');
    update_option('pewc_payment_id', '82200');
    update_option('pewc_license_id', '1100');
    update_option('pewc_license_level', '0');
    update_option('pewc_license_key', '919378767451da');
    delete_option('pewc_license_status_message');

    if (get_option('pewc_license_level') == 1) {
        update_option('pewc_license_level', '0');
    }
}

if (is_plugin_active('wp-rocket/wp-rocket.php')) {
    // Your license KEY.
    if (!defined('WP_ROCKET_KEY')) {
        define('WP_ROCKET_KEY', '7e7278c1');
    }

    // Your email, the one you used for the purchase.
    if (!defined('WP_ROCKET_EMAIL')) {
        define('WP_ROCKET_EMAIL', 'patai@patai.es');
    }
}
