<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if (is_plugin_active('supreme-modules-pro-for-divi/supreme-modules-pro-for-divi.php')) {

    if (!function_exists('get_hello_data')) {
        function get_hello_data($key, array $arr)
        {
            $val = array();
            array_walk_recursive($arr, function ($v, $k) use ($key, &$val) {
                if ($k == $key) {
                    array_push($val, $v);
                }
            });
            return count($val) > 1 ? $val : array_pop($val);
        }
    }

    $hellopack_updater_api_settings_key = get_option('hellopack_updater_api_settings');

    $license_data['key']        = get_hello_data('api_key', $hellopack_updater_api_settings_key);
    $license_data['last_check'] = time();

    update_site_option('dsm_pro_license', $license_data);
}
