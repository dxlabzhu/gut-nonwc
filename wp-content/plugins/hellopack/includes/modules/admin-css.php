<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

add_action('admin_head', 'admin_css_hpack');

function admin_css_hpack()
{
    echo '<style>
  .notice-otgs{display: none; }
  </style>';
}
