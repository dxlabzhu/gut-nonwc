<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
$hellopack_updater_instance = get_option('hellopack_updater_instance');
$hellopack_updater_api_settings_key = get_option('hellopack_updater_api_settings');
if (!function_exists('get_hello_data')) {
    function get_hello_data($key, array $arr)
    {
        $val = array();
        array_walk_recursive($arr, function ($v, $k) use ($key, &$val) {
            if ($k == $key) {
                array_push($val, $v);
            }
        });
        return count($val) > 1 ? $val : array_pop($val);
    }
}

if (is_plugin_active('elementor-pro/elementor-pro.php')) {
    if (get_option('elementor_pro_license_key') != get_option('elementor_hellopack_license_key')) {
        delete_option('elementor_pro_license_key');
        delete_option('_elementor_pro_license_data_fallback');
        delete_option('_elementor_pro_license_data');
    }

    if (!get_option('elementor_hellopack_license_key')) {
        delete_option('elementor_pro_license_key');
        delete_option('_elementor_pro_license_data_fallback');
        delete_option('_elementor_pro_license_data');
        add_option('elementor_hellopack_license_key', get_hello_data('api_key', $hellopack_updater_api_settings_key), '', 'yes');
    }

    if (!get_option('elementor_pro_license_key')) {
        add_option('elementor_pro_license_key', get_hello_data('api_key', $hellopack_updater_api_settings_key), '', 'yes');
    }
    if (!get_option('_elementor_pro_api_requests_lock')) {
        delete_option('_elementor_pro_api_requests_lock');
    }
    


    if (is_plugin_active('hellopack/hellopack-updater.php')) {
        if (ELEMENTOR_PRO_VERSION >= "3.2.2") {
            require_once('elementor/api.php');
            require_once('elementor/updater.php');
        } else {
            require_once('elementor/pre322/api.php');
        }
        require_once('elementor/base-app.php');
    }
} else {
    if (get_option('elementor_pro_license_key')) {
        delete_option('elementor_pro_license_key');
        delete_option('_elementor_pro_license_data_fallback');
        delete_option('_elementor_pro_license_data');
    }
}

/* Fix PRO widgets */

if (get_option('_elementor_pro_api_requests_lock') != 1) {
    $data = array('get_license_data'=>1735686000,'get_version'=>1735686000);
    update_option('_elementor_pro_api_requests_lock', $data);
}
