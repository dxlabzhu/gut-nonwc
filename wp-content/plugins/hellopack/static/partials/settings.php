<?php
defined('ABSPATH') || die(-1);
$settings_tabs = $this->get_tab_settings();
$current_tab = isset($_GET['tab']) ? hp_clean($_GET['tab']) : HPack_Admin::TAB_ACTIVATION;
$tab = isset($_GET['tab']) ? hp_clean($_GET['tab']) : HPack_Admin::TAB_ACTIVATION;
//$tab = $current_tab = HPOM()->api_key_exists() ? $tab : HPack_Admin::TAB_ACTIVATION;

?>
<div class="wrap">
    <div id="icon-themes" class="icon32"></div>
    <h2 class="dashboard"><span class="logo"><svg style="margin-right: 10px;" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="cube" width="36" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-cube fa-w-16 fa-3x"><path fill="currentColor" d="M239.1 6.3l-208 78c-18.7 7-31.1 25-31.1 45v225.1c0 18.2 10.3 34.8 26.5 42.9l208 104c13.5 6.8 29.4 6.8 42.9 0l208-104c16.3-8.1 26.5-24.8 26.5-42.9V129.3c0-20-12.4-37.9-31.1-44.9l-208-78C262 2.2 250 2.2 239.1 6.3zM256 68.4l192 72v1.1l-192 78-192-78v-1.1l192-72zm32 356V275.5l160-65v133.9l-160 80z" class=""></path></span></svg><?php _e('HelloPack Dashboard', 'hellopack')?></h2>
	<?php settings_errors();?>
    <div id="custom_errors_container"></div>

    <h2 class="nav-tab-wrapper">
        <?php
foreach ($settings_tabs as $tab_key => $tab_name):
	$active_tab = $current_tab === $tab_key ? ' nav-tab-active' : '';
	echo '<a class="nav-tab' . $active_tab . '" href="?page=' . HPack_Admin::SLUG_SETTINGS . '&tab=' . $tab_key . '">' . $tab_name . '</a>';
endforeach;
?>
    </h2> <!-- .nav-tab-wrapper -->

<?php if ($current_tab === HPack_Admin::TAB_ACTIVATION): ?>
    <form method="POST" action="">
<?php else: ?>
    <form method="POST" action="options.php">
<?php endif;?>
       <div id="hellopack_settings_main" class="main">
           <?php
if ($current_tab === HPack_Admin::TAB_ACTIVATION) {
	settings_fields(HPack_Settings_Manager::API_SETTINGS_MAIN);
	do_settings_sections(HPack_Admin::TAB_ACTIVATION);?>
                   <button type="button" id="save_api_settings" class="button button-primary" data-nonce="<?php echo wp_create_nonce('hp_cleanup_settings'); ?>"><?php _e('Activate API Key', 'hellopack');?></button>
			   <?php } elseif ($current_tab === HPack_Admin::TAB_OTHERS) {
	settings_fields(HPack_Settings_Manager::EXTRA_SETTINGS_KEY);
	do_settings_sections(HPack_Admin::TAB_OTHERS);
	submit_button(__('Save Changes', 'hellopack'));
}
?>
       </div>
    </form>
</div> <!-- .wrap -->
